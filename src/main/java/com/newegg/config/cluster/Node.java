package com.newegg.config.cluster;

public class Node {
	String host;
	String ip;
	Integer port;
	boolean isLeader;
	
	public Node(String host, String ip, Integer port, boolean isLeader) {
		super();
		this.ip = ip;
		this.host = host;
		this.port = port;
		this.isLeader = isLeader;
	}
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public boolean isLeader() {
		return isLeader;
	}
	public void setLeader(boolean isLeader) {
		this.isLeader = isLeader;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
}
