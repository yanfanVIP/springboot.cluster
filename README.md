## spring-boot-starter-cluster

-------------
## 引用依赖
```xml
<parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-parent</artifactId>
    <version>2.2.1.RELEASE</version>
</parent> 
	
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-cluster</artifactId>
</dependency>
```

## 示例

### Example1.java
```
package org.springframework.boot.cluster.example;

import javax.annotation.PostConstruct;
import javax.cache.Cache;
import org.apache.ignite.Ignite;
import org.apache.ignite.spi.discovery.tcp.ipfinder.TcpDiscoveryIpFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.cluster.util.IgniteClusterHelp;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("org.springframework.boot.cluster.**")
@SpringBootApplication
public class Example1 {
	
	static String appNameString = "example1";
	static String zookeeper = "10.16.236.126";
	
	public static void main(String[] args) {
		System.setProperty("spring.cluster.enabled", "true");
		System.setProperty("spring.application.name", appNameString);
		System.setProperty("server.port", "8080");
		System.setProperty("spring.cluster.zookeeper", zookeeper);
		System.setProperty("spring.cluster.cache.enable", "true");
		SpringApplication.run(Example1.class, args);
		TcpDiscoveryIpFinder finder = IgniteClusterHelp.getFinder(appNameString, zookeeper);
		try {
			System.out.println(finder.getRegisteredAddresses());
		} finally {
			finder.close();
		}
	}
	
	@Autowired
	Ignite ignite;
	
	@Autowired
	Cache<String, Object> cache;
	
	@PostConstruct
	void echo() {
		System.out.println("this cluster node is " + ignite.name());
		System.out.println("exist : " + cache.containsKey("a"));
		cache.put("a", "value : a");
		System.out.println("a = " + cache.get("a"));
	}
}
```

### Example2.java
```
package org.springframework.boot.cluster.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@ComponentScan("org.springframework.boot.cluster.**")
@SpringBootApplication
@EnableCaching
@RestController
public class Example2 {
	
	static String appNameString = "example1";
	static String zookeeper = "10.16.236.126";
	
	public static void main(String[] args) {
		System.setProperty("spring.cluster.enabled", "true");
		System.setProperty("spring.application.name", appNameString);
		System.setProperty("server.port", "8080");
		System.setProperty("spring.cluster.zookeeper", zookeeper);
		System.setProperty("spring.cluster.cache.enable", "true");
		SpringApplication.run(Example2.class, args);
	}
	
	@Autowired
	CacheDemo cache;
	
	@GetMapping("/echo/{message}")
	public String echo(@PathVariable String message) {
		return cache.getMessage(message);
	}
	
}
```

### CacheDemo.java
```
package org.springframework.boot.cluster.example;

import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = "data")
public class CacheDemo {

	@Cacheable(key="#message")
	public String getMessage(String message) {
		System.out.println("get message:" + message);
		return "message:" + message;
	}
	
}
```
--------
## 如何使用
1. 使用Maven引入依赖包，并作相关配置项
2. 若要结合SpringCache 可使用注解 @EnableCaching 开启
3. 本服务兼容JCache/SpringCache

### 配置参数详解
```
#[可选] web端口
#集群内部通信端口为server.port + 10
server.port=8080
#[必选] 是否开启cluster集群
spring.cluster.enabled=<true/false>
#[必选] 集群服务名称
spring.application.name=<example1>
#[必选] Zookeeper链接地址
spring.cluster.zookeeper=<localhost:2181>
```

### 连接到远程集群查询服务状态
```
TcpDiscoveryIpFinder finder = IgniteClusterHelp.getFinder(appNameString, zookeeper);
try {
	System.out.println(finder.getRegisteredAddresses());
} finally {
	finder.close();
}
```
